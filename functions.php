<?php 

// Change-Detector-XXXXXXXXXXXXX - for Espresso.app

/* Allow Automatic Updates
 ******************************
 * http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */

add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
add_filter( 'allow_major_auto_core_updates', '__return_true' );


add_action( 'after_setup_theme', 'my_child_theme_setup' );
function my_child_theme_setup() {
    
    load_theme_textdomain( 'organicthemes', get_stylesheet_directory() . '/lang' );
    
    // note: thumbnails in MOKA:
    // add_image_size( 'recentposts-widget-img', 300, 300, true );
    // add_image_size( 'recent-img', 360, 480, true );
    // add_image_size( 'slider-img', 1070, 600, true );
    add_image_size( 'poster-img', 300, 425, true );
    add_image_size( 'page-banner', 1600, 320, true );
    
}


function remove_menus(){
  
  remove_menu_page( 'edit.php?post_type=team' ); //Team
  remove_menu_page( 'edit-comments.php' ); //Comments
  
}
add_action( 'admin_menu', 'remove_menus' );

function custom_read_more() {
    return '... <a class="read-more" href="'.get_permalink(get_the_ID()).'">Plus d\'informations</a>';
}
function excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit, custom_read_more());
}



/*
 * Custom Sizes
 **************************
*/



/* Widget Areas
******************************/

register_sidebar( array(
		'name'          => 'Pied de menu',
		'id'            => 'menu-bottom',
		'description'   => 'Zone éditable en pied de menu.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
) );


function tangomoon_register_fonts() {
	$protocol = is_ssl() ? 'https' : 'http';
	wp_register_style( 'tangomoon_alegreya', "$protocol://fonts.googleapis.com/css?family=Alegreya+Sans:400,700|Alegreya:400italic,400,700" );
	
}
add_action( 'init', 'tangomoon_register_fonts' );


function custom_register_styles() {

	/**
	 * Custom CSS
	 */

	
	if ( WP_DEBUG == true ) {
	
			// DEV: the MAIN stylesheet - uncompressed
			wp_enqueue_style( 
					'main-style', 
					get_stylesheet_directory_uri() . '/css/dev/00-main.css', // main.css
					false, // dependencies
					null // version
			); 
	
	} else {
	
			// PROD: the MAIN stylesheet - combined and minified
			wp_enqueue_style( 
					'main-style', 
					get_stylesheet_directory_uri() . '/css/build/styles.20171211134531.css', // main.css
					false, // dependencies
					null // version
			); 
	}


	// wp_dequeue_style( 'collective-style' ); no effect....
//	
	wp_dequeue_style( 'collective_arimo' );
	wp_dequeue_style( 'collective_lora' );
	wp_dequeue_style( 'collective_archivo_narrow' );
	wp_dequeue_style( 'collective_oswald' );
	
	
	wp_enqueue_style( 'tangomoon_alegreya' );
	
//	
//	wp_dequeue_script( 'moka-flex-slider' );
	
	wp_enqueue_script( 
	// the MAIN JavaScript file -- development version
			'main-script',
			get_stylesheet_directory_uri() . '/js/scripts.js', // scripts.js
			array('jquery'), // dependencies
			null, // version
			true // in footer
	);

}
add_action( 'wp_enqueue_scripts', 'custom_register_styles', 21 );



/* admin interface
******************************/

if ( is_user_logged_in() ) {
		require_once('functions-admin.php');
}


/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}


/*
 * File Upload Security
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}


/* Jetpack Stuff
* see: http://jeremyherve.com/2013/11/19/customize-the-list-of-modules-available-in-jetpack/
 * Disable all non-whitelisted jetpack modules.
 *
 * This will allow all of the currently available Jetpack modules to work
 * normally. If there's a module you'd like to disable, simply comment it out
 * or remove it from the whitelist and it will no longer load.
 *
 * @author FAT Media, LLC
 * @link   http://wpbacon.com/tutorials/disable-jetpack-modules/
 */
 
 
 function tangomoon_jetpacks( $modules ) {
 	// A list of Jetpack modules which are allowed to activate.
 	$whitelist = array(
// 		'after-the-deadline',
 		'carousel',
// 		'comments',
// 		'contact-form',
// 		'custom-css',
 		'enhanced-distribution',
// 		'gplus-authorship',
// 		'gravatar-hovercards',
// 		'infinite-scroll',
// 		'json-api',
// 		'latex',
// 		'likes',
//		'markdown',
// 		'minileven',
// 		'mobile-push',
 		'monitor',
// 		'notes',
// 		'omnisearch',
// 		'photon',
// 		'post-by-email',
  	'protect',
 		'publicize',
 		'sharedaddy',
// 		'shortcodes',
// 		'shortlinks',
// 		'sso',
 		'stats',
// 		'subscriptions',
 		'tiled-gallery',
// 		'vaultpress',
// 		'videopress',
 		'widget-visibility',
// 		'widgets',
 	);
 	// Deactivate all non-whitelisted modules.
 	$modules = array_intersect_key( $modules, array_flip( $whitelist ) );
 	return $modules;
}

  add_filter( 'jetpack_get_available_modules', 'tangomoon_jetpacks' );
  
  add_filter( 'jetpack_get_default_modules', 'tangomoon_jetpacks' );


function my_mem_settings() {
	mem_plugin_settings( array( 'post' ), 'full' );
}

add_action( 'mem_init', 'my_mem_settings' );

function remove_jetpack_sharing() {
    if ( is_singular() && function_exists( 'sharing_display' ) ) {
        remove_filter( 'the_excerpt', 'sharing_display', 19 );
    }
}
add_action( 'loop_start', 'remove_jetpack_sharing' );
