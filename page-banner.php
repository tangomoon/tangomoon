<?php
/**
Template Name: Tangomoon - Page banner
* This template displays the default page content.
*
* @package Collective
* @since Collective 1.0
*
*/
get_header(); ?>

<?php $thumb = ( '' != get_the_post_thumbnail() ) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'page-banner' ) : false; ?>

<!-- BEGIN .post class -->
<div <?php post_class(); ?> id="page-<?php the_ID(); ?>">
	
	<!--<h1 class="headline page-headline text-center hidden"><?php the_title(); ?></h1>-->
	
	<?php $feature_page = get_theme_mod( 'page_feature' ); 
	
	 if ( has_post_thumbnail() && ! is_page( $feature_page ) ) { ?>
		<div class="feature-img page-banner" <?php if ( ! empty( $thumb ) ) { ?> style="background-image: url(<?php echo $thumb[0]; ?>);" <?php } ?>>
			<?php the_post_thumbnail( 'page-banner' ); ?>
		</div>
	<?php } ?>
	
	<!-- BEGIN .row -->
	<div class="row">
	

			<?php if ( is_active_sidebar( 'page-sidebar' ) ) : ?>
			
			<!-- BEGIN .content -->
			<div class="content">
			
				<!-- BEGIN .eleven columns -->
				<div class="eleven columns">
		
					<!-- BEGIN .postarea -->
					<div class="postarea">
  										
						<?php get_template_part( 'loop', 'page' ); ?>
					
					<!-- END .postarea -->
					</div>
				
				<!-- END .eleven columns -->
				</div>
				
				<!-- BEGIN .five columns -->
				<div class="five columns">
				
					<?php get_sidebar(); ?>
					
				<!-- END .five columns -->
				</div>
		
			<?php else : ?>
		
		
			<?php 
			
			if ($post->post_content!="") : ?>
			
			<!-- BEGIN .content -->
			<div class="content main-page-content">
		
				<!-- BEGIN .sixteen columns -->
				<div class="sixteen columns">
		
					<!-- BEGIN .postarea full -->
					<div class="postarea full">
  					
  					<h1 class="headline"><?php the_title(); ?></h1>
			
						<?php get_template_part( 'loop', 'page' ); ?>
					
					<!-- END .postarea full -->
					</div>
					
				<!-- END .sixteen columns -->
				</div>
				
			<!-- END .content -->
			</div>
					
			<?php endif; // post_content not empty ?>
		
			<?php 
				endif; // is_active_sidebar 
			?>
		
	
	<!-- END .row -->
	</div>
	
<!-- END .post class -->
</div>

<?php get_footer(); ?>