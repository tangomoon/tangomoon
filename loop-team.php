<!-- BEGIN .members -->
<div class="members">

<?php if ( is_home() && '-1' != get_theme_mod( 'category_team_home' ) || is_home() && '' != get_theme_mod( 'category_team_home' ) ) { ?>

	<?php 
	
//	$team = new WP_Query(array(
//		'post_type' => 'team', 
//		'posts_per_page' => -1, 
//		'suppress_filters'=>0, 
//		'tax_query' => array(
//	        array(
//		        'taxonomy' => 'category-team',
//		        'field' => 'id',
//		        'terms' => get_theme_mod( 'category_team_home' ),
//	        ))
//	    )); 
	    
	    $team = new WP_Query(	array(
	    	'category_name' => 'a-la-une',
	    	'post_type' => 'post',
	    	'posts_per_page' => 4,
	    	));
	    
	    ?>
	
<?php }  ?>

<?php if ($team->have_posts()) : while($team->have_posts()) : $team->the_post(); ?>
<?php global $more; $more = 0; ?>
	
<!-- BEGIN .four columns -->
<div class="four columns">
	
	<!-- BEGIN .holder -->
	<div class="holder">
		<a class="block-link" href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) { ?>
			<?php the_post_thumbnail( 'thumbnail' ); ?>
		<?php
		
		 } else {
		 
		 ?>
		 
		 <img src="//www.tangomoon.ch/wp-content/uploads/2014/10/20141101_210338-e1417192591148-150x128.jpg" class="attachment-thumbnail wp-post-image" alt="20141101_210338" scale="0">
		 
		 <?php
		 
		 
		 }
		 
		 
		  ?>
		
		<!-- BEGIN .information -->
		<div class="information">
		
			<h2 class="title text-center"><?php the_title(); ?></h2>
			
			<?php if ( ! empty( $post->post_excerpt ) || get_post_meta($post->ID, 'team_title', true) ) { ?>
				
				<div class="excerpt">
				
					<?php if ( get_post_meta($post->ID, 'team_title', true) ) : ?>
						<p class="subtitle"><?php echo get_post_meta(get_the_ID(), 'team_title', true); ?></p>
					<?php endif; ?>
					
					<span class="border-line"></span>
					
					<?php if ( ! empty( $post->post_excerpt ) ) { ?>
						<?php the_excerpt(); ?>
					<?php } ?>
					
				</div>
			
			<?php } ?>
		
		<!-- END .information -->
		</div>
		</a>
	<!-- END .holder -->
	</div>

<!-- END .four columns -->
</div>

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<!-- END .members -->
</div>