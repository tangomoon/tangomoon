<?php
/**
* This template is used to display the home page.
*
* @package Collective
* @since Collective 1.0
*
*/
get_header(); ?>

<!-- BEGIN .post class -->
<div <?php post_class("site-intro"); ?> id="page-<?php the_ID(); ?>">

	
		<?php get_template_part( 'content/home', 'page' ); ?>
	
	
				<?php // get_template_part( 'loop', 'team' ); ?>
		
	
	<!-- BEGIN .row -->
	<div class="row news-section" <?php if ( '' != get_theme_mod( 'news_background' ) ) { ?> style="background-image: url(<?php echo get_theme_mod('news_background') ?>); background-repeat: repeat;" <?php } ?>>
		
		<!-- BEGIN .content -->
		<div class="content">
				
			<!-- BEGIN .featured-news -->
			<div class="featured-news">
						
				<?php get_template_part( 'content/home', 'news' ); ?>
		
			<!-- END .featured-news -->
			</div>
						
		<!-- END .content -->
		</div>
	<!-- END .row -->
	</div>
	

<!-- END .post class -->
</div>

<?php get_footer(); ?>