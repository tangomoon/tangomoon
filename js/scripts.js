;
(function ($) {
	// Declare Flexslider function, in order to prevent JS errors.
	$.fn.flexslider = function(options) {};
})(jQuery);
  
  
jQuery(document).ready(function($){
		
		/* 
		 * Outgoing Links = new window
		 ****************************************************
		 */
		
		$("a[href^=http]").each(
		   function(){
		      if(this.href.indexOf(location.hostname) === -1) {
		      $(this).attr('target', '_blank');
					}
		   }
		 );
		 
		 /* 
		  * 1.
		  * EmailSpamProtection (jQuery Plugin)
		  ****************************************************
		  * Author: Mike Unckel
		  * Description and Demo: http://unckel.de/labs/jquery-plugin-email-spam-protection
		  * HTML: <span class="email">info [at] domain.com</span>
		  */
		 $.fn.emailSpamProtection = function(className) {
		 	return $(this).find("." + className).each(function() {
		 		var $this = $(this);
		 		var s = $this.text().replace(" [at] ", "&#64;");
		 		$this.html("<a href=\"mailto:" + s + "\">" + s + "</a>");
		 	});
		 };
		 $("body").emailSpamProtection("email");
		 
		 /* 
		  * Show Nom+Prenom when click
		  ****************************************************
		  */
		$( "#form-wysija-2 input" ).focus(function() {
		  // alert( "Handler for .focus() called." );
		  $( "#form-wysija-2 .wysija-paragraph:nth-of-type(2), #form-wysija-2 .wysija-paragraph:nth-of-type(3)" ).show(200);
		});  
		
		$( ".footer-widgets .mailpoet_form_widget .mailpoet_paragraph:nth-of-type(2), .footer-widgets .mailpoet_form_widget .mailpoet_paragraph:nth-of-type(3)" ).hide();
		$( ".footer-widgets .mailpoet_form_widget input" ).focus(function() {
		  // alert( "Handler for .focus() called." );
		  $( ".footer-widgets .mailpoet_form_widget .mailpoet_paragraph:nth-of-type(2), .footer-widgets .mailpoet_form_widget .mailpoet_paragraph:nth-of-type(3)" ).show(200);
		});  
		
		 
		
}); // end document ready