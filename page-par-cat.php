<?php
/**
Template Name: Tangomoon - par Catégorie
* This template displays the default page content.
*
* @package Collective
* @since Collective 1.0
*
*/
get_header(); ?>

<?php $thumb = ( '' != get_the_post_thumbnail() ) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'page-banner' ) : false; ?>

<!-- BEGIN .post class -->
<div <?php post_class(); ?> id="page-<?php the_ID(); ?>">
	
	<!--<h1 class="headline page-headline text-center hidden"><?php the_title(); ?></h1>-->
	
	<?php $feature_page = get_theme_mod( 'page_feature' ); 
	
	 if ( has_post_thumbnail() && ! is_page( $feature_page ) ) { ?>
		<div class="feature-img page-banner" <?php if ( ! empty( $thumb ) ) { ?> style="background-image: url(<?php echo $thumb[0]; ?>);" <?php } ?>>
			<?php the_post_thumbnail( 'page-banner' ); ?>
		</div>
	<?php } ?>
	
	<!-- BEGIN .row -->
	<div class="row">
	

			<?php if ( is_active_sidebar( 'page-sidebar' ) ) : ?>
			
			<!-- BEGIN .content -->
			<div class="content">
			
				<!-- BEGIN .eleven columns -->
				<div class="eleven columns">
		
					<!-- BEGIN .postarea -->
					<div class="postarea">
  										
						<?php get_template_part( 'loop', 'page' ); ?>
					
					<!-- END .postarea -->
					</div>
				
				<!-- END .eleven columns -->
				</div>
				
				<!-- BEGIN .five columns -->
				<div class="five columns">
				
					<?php get_sidebar(); ?>
					
				<!-- END .five columns -->
				</div>
		
			<?php else : ?>
		
		
			<?php 
			
			if($post->post_content!="") {
			
			 ?>
			<!-- BEGIN .content -->
			<div class="content main-page-content">
		
				<!-- BEGIN .sixteen columns -->
				<div class="sixteen columns">
		
					<!-- BEGIN .postarea full -->
					<div class="postarea full">
  					
  					<h1 class="headline"><?php the_title(); ?></h1>
			
						<?php get_template_part( 'loop', 'page' ); ?>
					
					<!-- END .postarea full -->
					</div>
					
				<!-- END .sixteen columns -->
				</div>
				
			<!-- END .content -->
			</div>
					
			<?php 
			
				$liste_par_cat_class = "with-intro";
			
			} else {
			
				$liste_par_cat_class = "no-intro";
			
			}
			
					
					// Extra Loop: Get Articles By Category!
					
					// get page slug = category.
					
					$page_id = get_post($post->ID); // get current post
					
					$query_object = 'category_name';
					$query_content =  $page_id->post_name; // define page slug
					
					echo '';
					
					// Loop 1: items with EVENT DATE
					
					$custom_query = new WP_Query( array( 
								'posts_per_page' => -1,
								$query_object => 	$query_content,
								'meta_key' => '_mem_start_date',
								
								//'orderby' => 'meta_value',
								//'order' => 'ASC',
								
								 ));
					
					
					if ( $custom_query->have_posts() ) : ?>
					
					<div class="liste-par-cat <?php echo $liste_par_cat_class; ?>">
  					
  					<?php $next_courses_title = get_post_meta($post->ID, 'titre_prochains_cours', true); ?>
  					<?php if ( ! empty ( $next_courses_title ) ) : ?>
  					  <header class="block-header">
    			      <h2 class="block-title"><?php echo $next_courses_title ?></p>
  					  </header>
    			  <?php endif; ?>
  							 
  						<?php 
							  
							  while( $custom_query->have_posts() ) : $custom_query->the_post();
									    						     						    						 
									// include( TEMPLATEPATH . '/inc/generate-array.php' );
									?>
						 		  <!-- BEGIN .article -->
						 		  <div class="article content">						 		  	
						 		  	
						 		  	<?php 
						 		  	
						 		  	$thumb = ( '' != get_the_post_thumbnail() ) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'collective-featured-small' ) : false; 
						 		  	
						 		  	
						 		  	 if ( has_post_thumbnail() ) { ?>
						 		  			<div class="feature-img four columns" <?php if ( ! empty( $thumb ) ) { ?> style="background-image: url(<?php echo $thumb[0]; ?>);" <?php } ?>>
						 		  				<?php the_post_thumbnail( 'collective-featured-small' ); ?>
						 		  			</div>
						 		  		<?php
						 		  				
						 		  				$thumbnail_status = "has-thumbnail";
						 		  		
						 		  		 } else {
						 		  		 
						 		  		 		$thumbnail_status = "no-thumbnail";
						 		  		 }
						 		  		 
						 		  		 ?>
						 		  		<div class="article-content twelve columns">
  						 		  		<h2 class="headline <?php echo $thumbnail_status; ?>">
    						 		  		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    						 		  	</h2>
    						 		  	<div class="excerpt">
                					<?php echo excerpt(34); ?>
                				</div>
						 		  		</div> <!-- .article-content -->
						 		  <!-- END .article -->
						 		  </div>
						 		  
						 		  <div class="clear"></div>
						 		  
						 		  <?php
						 endwhile; 
						 
						?></div><?php // .liste-par-cat
						
					endif; // custom_query
					 ?>
		
			<?php 
				endif; // is_active_sidebar 
			?>
		
	
	<!-- END .row -->
	</div>
	
<!-- END .post class -->
</div>
<?php get_footer(); ?>